# Responsive Web Design

### Lets Discuss What Is Responsiveness Of Site

- We know in the current trend there are lots of users using smartphones, tablets, computers, and even wearable devices to browse websites. The problem here is the size of the screen and the viewport of the devices
- Back in the day, all the websites were designed to be viewed only on a desktop. to solve this problem a responsive web design came into the picture
- Web pages that render well on a variety of devices, this makes a web page responsive.
- Responsive web design makes your web page look good on all devices.
- Responsive web design uses only HTML and CSS.
- Responsive web design is not a program or a JavaScript.

### The Concept Of Responsive Web Design

- Web pages can be viewed using many different devices: desktops, tablets, and phones. Your web page should look good, and be easy to use, regardless of the device.
- Web pages should not leave out information to fit smaller devices, but rather adapt its content to fit any device . As shown in the below images that fits to the desktop,tablet and smartphone screen size.
  > ![img-desktop](https://www.w3schools.com/css/rwd_desktop.png) ![img-tablet](https://www.w3schools.com/css/rwd_tablet.png) ![img-smartphone](https://www.w3schools.com/css/rwd_phone.png)

### What is The Viewport

- The viewport is the user's visible area of a web page.
- HTML5 introduced a method to let web designers take control over the viewport, through the <meta> tag.
  > `<meta name="viewport" content="width=device-width, initial-scale=1.0">`
- The width=device-width part sets the width of the page to follow the screen-width of the device (which will vary depending on the device).
- The initial-scale=1.0 part sets the initial zoom level when the page is first loaded by the browser.

### What is a Media Query

- Media Query is a modern way to design a responsive webpage using css3
- Media query is a CSS technique introduced in CSS3.
- It uses the @media rule to include a block of CSS properties only if a certain condition is true.

Example for media query  
If the browser window is 600px or smaller, the background color will be lightblue:

```css
@media only screen and (max-width: 600px) {
  body {
    background-color: lightblue;
  }
}
```

### For Further More Information About Responsive Web Design Vist Following Links.

[w3Schools text tutorials](https://www.w3schools.com/css/css_rwd_intro.asp)

[Responsive Web Design - What It Is And How To Use It](https://www.smashingmagazine.com/2011/01/guidelines-for-responsive-web-design/)

[Responsive design MDN Docs](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Responsive_Design)

# THANK YOU HAPPY CODING!
